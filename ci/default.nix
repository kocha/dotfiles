{lib, ...}: {
  ci.gitlab.default.jobs.renovate-config-validator.jobConfiguration.script = lib.mkForce [
    "nix shell --inputs-from . slagit-nix#renovate -c renovate-config-validator"
  ];
}
