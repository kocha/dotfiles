{
  description = "My Home Manager Flake";

  inputs = {
    flake-parts.url = "github:hercules-ci/flake-parts";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    home-manager = {
      url = "github:nix-community/home-manager/release-24.05";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix-index-database = {
      url = "github:Mic92/nix-index-database";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    slagit-nix.url = "gitlab:slagit/mail";
  };

  outputs = inputs @ {
    self,
    flake-parts,
    home-manager,
    slagit-nix,
    ...
  }:
    flake-parts.lib.mkFlake {inherit inputs;} {
      imports = [
        slagit-nix.ci.flakeModule
        ./home.nix
        ./ci
      ];
      perSystem = {inputs', ...}: {
        packages.default = inputs'.home-manager.packages.default;
      };
      systems = ["aarch64-linux" "x86_64-linux"];
    };
}
