{
  self,
  inputs,
  withSystem,
  ...
}: let
  mkConfig = system: cfg: withSystem system (args @ {pkgs, ...}: inputs.home-manager.lib.homeManagerConfiguration ({inherit pkgs;} // (cfg args)));
in {
  flake = {
    homeConfigurations = {
      "albert@cyclone" = mkConfig "x86_64-linux" (args: {
        modules = [
          inputs.nix-index-database.hmModules.nix-index
          inputs.slagit-nix.hmModules.sv
          self.hmModules.common
          self.hmModules.workstation
          self.hmModules.x11
          ./users/albert.nix
        ];
      });
      "albert@franklin" = mkConfig "x86_64-linux" (args: {
        modules = [
          inputs.nix-index-database.hmModules.nix-index
          self.hmModules.common
          ./users/albert.nix
        ];
      });
      "albert@unifi" = mkConfig "aarch64-linux" (args: {
        modules = [
          inputs.nix-index-database.hmModules.nix-index
          self.hmModules.common
          ./users/albert.nix
        ];
      });
    };
    hmModules = {
      common = ./modules/common.nix;
      workstation = ./modules/workstation.nix;
      x11 = ./modules/x11.nix;
    };
  };
}
