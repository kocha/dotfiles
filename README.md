# Dotfiles for Nix Home Manager

To rebuild home configuration:

```bash
nix run . switch -- --flake ".#albert@$(hostname)"
```

or, without checking out, run:

```bash
nix run gitlab:kocha/dotfiles switch -- --flake "gitlab:kocha/dotfiles#albert@$(hostname)"
```
