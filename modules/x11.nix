{pkgs, ...}: let
  cfg-home = pkgs.writeScriptBin "cfg-home" ''
    #!${pkgs.runtimeShell}

    set -eu

    exec ${pkgs.xorg.xrandr}/bin/xrandr --output HDMI-1 --auto --right-of eDP-1
  '';
  cfg-roaming = pkgs.writeScriptBin "cfg-roaming" ''
    #!${pkgs.runtimeShell}

    set -eu

    exec ${pkgs.xorg.xrandr}/bin/xrandr --output HDMI-1 --off
  '';
in {
  home = {
    file.".ratpoisonrc".text = ''
      exec exec ${pkgs.xorg.xsetroot}/bin/xsetroot -solid black -cursor_name left_ptr
      exec exec ${pkgs.wmname}/bin/wmname LG3D

      set msgwait 3
      set rudeness 12
      set border 0
      set bargravity c
      set bgcolor yellow
      set startup_message off
      set warp on

      exec exec ${pkgs.st}/bin/st -e ${pkgs.tmux}/bin/tmux
      banish

      escape C-o

      bind a exec exec sh ${pkgs.ratpoison.contrib}/share/ratpoison/ratdate.sh
      bind b banishrel
      bind c exec exec ${pkgs.st}/bin/st -e ${pkgs.tmux}/bin/tmux
      bind C exec exec ${pkgs.st}/bin/st
      bind j nextscreen
      bind v ratclick 2

      exec exec ${pkgs.xorg.xset}/bin/xset s 300 5
      exec exec ${pkgs.xss-lock}/bin/xss-lock -n ${pkgs.xsecurelock}/libexec/xsecurelock/dimmer -l -- ${pkgs.xsecurelock}/bin/xsecurelock
      bind x exec exec ${pkgs.xorg.xset}/bin/xset s activate
    '';
    packages = [
      cfg-home
      cfg-roaming
      pkgs.pavucontrol
      pkgs.ratpoison
    ];
  };
  programs = {
    firefox = {
      enable = true;
      profiles = {
        home = {
          id = 0;
          settings = {
            "browser.newtabpage.activity-stream.feeds.section.topstories" = false;
            "browser.startup.page" = 3;
            "datareporting.healthreport.uploadEnabled" = false;
            "datareporting.policy.dataSubmissionEnabled" = false;
            "datareporting.policy.dataSubmissionPolicyBypassNotification" = true;
            "signon.rememberSignons" = false;
          };
        };
      };
    };
  };
  xsession = {
    enable = true;
    windowManager.command = "${pkgs.ratpoison}/bin/ratpoison";
  };
}
