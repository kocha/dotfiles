{
  lib,
  pkgs,
  ...
}: {
  home = {
    packages = [
      pkgs.acpi
      pkgs.sv
      pkgs.zoom-us
    ];
    sessionVariables.VAULT_ADDR = "https://slagit-vault.net:8200";
  };
  nixpkgs.config.allowUnfreePredicate = pkgs:
    builtins.elem (lib.getName pkgs) [
      "zoom"
    ];
  programs = {
    git.signing = {
      key = "CA67DFDD46E83AEF6750B1BC21544809F004839F";
      signByDefault = true;
    };
    gpg.enable = true;
  };
  services.gpg-agent.enable = true;
}
