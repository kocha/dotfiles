{
  config,
  pkgs,
  ...
}: {
  home = {
    packages = [
      pkgs.ack
      pkgs.file
    ];
    sessionVariables.EDITOR = "vi";
    stateVersion = "22.11";
  };
  programs = {
    bash = {
      enable = true;
      historyControl = ["ignoredups"];
      initExtra = "set -o vi\n";
    };
    git = {
      enable = true;
      extraConfig = {
        core.pager = "${pkgs.delta}/bin/delta";
        delta.line-numbers = true;
        diff.algorithm = "histogram";
        init.defaultBranch = "main";
        merge.conflictstyle = "zdiff3";
        rebase = {
          autosquash = true;
          autostash = true;
        };
      };
      userName = "Albert Koch";
      userEmail = "kocha@slagit.net";
    };
    tmux = {
      enable = true;
      keyMode = "vi";
      shortcut = "a";
      extraConfig = "set-option -g status-style bg=black,fg=red\nset-option -g window-status-current-style bright,reverse\nset-option -g status-left \"\"\nset-option -g set-titles on\n";
    };
    vim = {
      enable = true;
      extraConfig = ''
        color desert
      '';
      plugins = [
        pkgs.vimPlugins.editorconfig-vim
      ];
    };
  };
}
